import { Component } from '@angular/core';
import { Platform, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { UsuarioService } from '../providers/usuario';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  home = HomePage;
  incidencias = "IncidenciasPage";

  rootPage:any = "LoginPage";

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private menuCtrl:MenuController,
              private _us:UsuarioService) {

    platform.ready().then(() => {
      this._us.cargarStorage()
          .then( ()=>{
            if( this._us.token ){
              this.rootPage = HomePage;
            }else{
              this.rootPage = "LoginPage";
            }
            statusBar.styleDefault();
            splashScreen.hide();

          })
    });
  }

  abrirPagina( pagina: any ){
    this.rootPage = pagina;
    this.menuCtrl.close();
  }
}
