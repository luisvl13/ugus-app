import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

import { AlertController, Platform, ToastController } from 'ionic-angular';

import { URLV1_SERVICES } from '../config/url.services';
import { UsuarioService } from './usuario';

@Injectable()
export class IncidenciasService {

  incidencias: any[] = [];

  constructor(public http: Http,
              private _us: UsuarioService,
              private toastCtrl:ToastController) {
    this.cargarIncidencias();
  }

  cargarFoto(archivo: InfoSubir) {


  }

  cargarIncidencias() {
    let url = URLV1_SERVICES + "incidencias";
    let headers = new Headers();
    headers.append('Authorization', `Bearer ${this._us.token}`);

    let options = new RequestOptions({ headers: headers });

    this.http.get(url, options)
      .map(resp => resp.json())
      .subscribe(data => {
        if (data.error) {
          //manejar error
        } else {
          this.incidencias.push(...data.data);
          console.log(this.incidencias)
        }

      });

  }

  private mostrarToast( texto:string ){
    this.toastCtrl.create({
      message: texto,
      duration: 2500
    }).present();
  }

}

interface InfoSubir{
  img:string;
  titulo:string;
}
