import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';

import { AlertController, Platform } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { URL_SERVICES } from '../config/url.services';

@Injectable()
export class UsuarioService {

  token: string;
  usuario: any = {};

  constructor(public http: Http,
    private alertCtrl: AlertController,
    private platform: Platform,
    private storage: Storage) {

    this.cargarStorage();

  }

  activo(): boolean {

    if (this.token) {
      return true;
    } else {
      return false;
    }

  }

  ingresar(correo: string, contrasena: string) {
    let data = new URLSearchParams();
    data.append("id", correo);
    data.append("password", contrasena);

    let url = URL_SERVICES + "obtener-token";

    let promesa = new Promise((resolve, reject) => {
      this.http.post(url, data)
        .subscribe(

          data => {
            let dataResp = data.json();
            this.token = dataResp.token;
            this.usuario = dataResp.usuario;
            //guardarStorage
            this.guardarStorage();
            resolve(true);
          },
          error => {
            resolve(error.status);
          });
    })
      .catch(error => {
        console.log("Error en promesa Service: " + JSON.stringify(error));
      })

    return promesa;
  }

  cerrarSesion() {
    this.token = null;
    this.usuario = null;

    //guardarStorage
    this.guardarStorage();
  }

  private guardarStorage() {
    let promesa = new Promise((resolve, reject) => {

      if (this.platform.is("cordova")) {
        //cel
        this.storage.set('token', this.token);
        this.storage.set('usuario', this.usuario);

      } else {
        //computadora
        if (this.token) {
          localStorage.setItem("token", this.token);
          localStorage.setItem("usuario", JSON.stringify(this.usuario));
        } else {
          localStorage.removeItem("token");
          localStorage.removeItem("usuario");
        }

      }
    });

    return promesa;

  }

  cargarStorage() {
    let promesa = new Promise((resolve, reject) => {

      if (this.platform.is("cordova")) {
        //cel
        this.storage.ready()
          .then(() => {
            this.storage.get("token")
              .then(token => {
                if (token) {
                  this.token = token;
                }
                resolve();
              });

            this.storage.get("usuario")
              .then(usuario => {
                if (usuario) {
                  this.usuario = usuario;
                }
                resolve();
              });
          });

      } else {
        //computadora
        if (localStorage.getItem("token")) {
          this.token = localStorage.getItem("token");
          this.usuario = JSON.parse(localStorage.getItem("usuario"));
          resolve();
        }
      }

    })

    return promesa;
  }

}
