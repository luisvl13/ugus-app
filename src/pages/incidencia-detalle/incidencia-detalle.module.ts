import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidenciaDetallePage } from './incidencia-detalle';

@NgModule({
  declarations: [
    IncidenciaDetallePage,
  ],
  imports: [
    IonicPageModule.forChild(IncidenciaDetallePage),
  ],
})
export class IncidenciaDetallePageModule {}
