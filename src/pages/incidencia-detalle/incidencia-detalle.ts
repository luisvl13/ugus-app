import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController  } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-incidencia-detalle',
  templateUrl: 'incidencia-detalle.html',
})
export class IncidenciaDetallePage {

    incidencia:any = {};

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalCtrl: ModalController) {

        this.incidencia = this.navParams.get( "incidencia" );
        console.log( this.incidencia );

  }

  presentModal() {
    let modal = this.modalCtrl.create("IncidenciaFotoModalPage");
    modal.present();
  }

}
