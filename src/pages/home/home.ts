import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { UsuarioService } from '../../providers/usuario';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController,
              private _us:UsuarioService) {
  }
}
