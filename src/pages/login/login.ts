import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';

import { HomePage } from '../home/home';

import { UsuarioService } from '../../providers/usuario';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private mensaje: string = "";
  public loginForm: any;
  public backgroundImage: any = "./assets/img/fondo-slogo.png";
  public imgLogo: any = "./assets/img/logo-stxt.svg";

  constructor(public navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private fb: FormBuilder,
              private _us: UsuarioService) {

    let EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;

    this.loginForm = fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(EMAIL_REGEXP)])],
      password: ['', Validators.compose([Validators.minLength(4), Validators.required])]
    });

  }

  login() {
    if (!this.loginForm.valid) {
      //this.presentAlert('Username password can not be blank')
      console.log("error");
    } else {
      let loadingPopup = this.loadingCtrl.create({
        spinner: 'crescent',
        content: ''
      });
      loadingPopup.present();

      this._us.ingresar(this.loginForm.value.email, this.loginForm.value.password)
        .then(authData => {

          try {
            if (authData == true){
              loadingPopup.dismiss();
              this.navCtrl.setRoot( HomePage );
            }

            if (authData == 401){
              this.mensaje = "Lo sentimos el usuario y/o contraseña no son válidos.";
            }

            if (authData == 0){
              this.mensaje = "Conexión rechazada."
            }

            if (authData == 500 ){
              this.mensaje = "500 (Error interno del servidor)";
            }
          } catch(e){
            if (authData == 500 ){
              this.mensaje = "500 (Error interno del servidor)";
            }
          }

          if(this.mensaje){
            loadingPopup.dismiss().then(() => {
              this.presentAlert(this.mensaje)
            });
          }
          console.log(authData);


        }, error => {

        });


    }
  }

  presentAlert(title) {
    let alert = this.alertCtrl.create({
      title: title,
      buttons: ['OK']
    });
    alert.present();
  }

}
