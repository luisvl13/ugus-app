import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidenciaFotoModalPage } from './incidencia-foto-modal';

@NgModule({
  declarations: [
    IncidenciaFotoModalPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidenciaFotoModalPage),
  ],
})
export class IncidenciaFotoModalPageModule {}
