import { Component } from '@angular/core';
import { IonicPage, ViewController, ToastController, Platform, LoadingController } from 'ionic-angular';

//plugins
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';

//servicios
import { IncidenciasService } from '../../providers/incidencias';

@IonicPage()
@Component({
  selector: 'page-incidencia-contrareferencia-modal',
  templateUrl: 'incidencia-contrareferencia-modal.html',
})
export class IncidenciaContrareferenciaModalPage {

  imgPreview: string = null;
  img: string = "";

  constructor(private viewCtrl: ViewController,
              private toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              private platform: Platform,
              private imagePicker: ImagePicker,
              private camera: Camera,
              private _is:IncidenciasService) {
  }


    seleccionarFotos() {

      if (!this.platform.is("cordova")) {
        this.mostrarToast("Error: No estamos en un celular");
        return;
      }

      let options: ImagePickerOptions = {
        maximumImagesCount: 1,
        quality: 40,
        outputType: 1
      }

      this.imagePicker.getPictures(options).then((results) => {

        for (let img of results) {
          this.imgPreview = 'data:image/jpeg;base64,' + img;
          this.img = img;
        }

      }, (err) => {
        this.mostrarToast("Error seleccion: " + err);
        console.error("Error seleccion: " + JSON.stringify(err));
      });
    }

    mostrarCamara() {

      if (!this.platform.is("cordova")) {
        this.mostrarToast("Error: No estamos en un celular");
        return;
      }

      const options: CameraOptions = {
        quality: 40,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        correctOrientation: true
      }

      this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64:
        this.imgPreview = 'data:image/jpeg;base64,' + imageData;
        this.img = imageData;

      }, (err) => {
        this.mostrarToast("Error: " + err);
        console.error("Error en la camara: ", err);
      });

    }

    private mostrarToast(texto: string) {
      this.toastCtrl.create({
        message: texto,
        duration: 2500
      }).present();
    }

    cerrarModal() {
      this.viewCtrl.dismiss();
    }

}
