import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncidenciaContrareferenciaModalPage } from './incidencia-contrareferencia-modal';

@NgModule({
  declarations: [
    IncidenciaContrareferenciaModalPage,
  ],
  imports: [
    IonicPageModule.forChild(IncidenciaContrareferenciaModalPage),
  ],
})
export class IncidenciaContrareferenciaModalPageModule {}
