import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { IncidenciasService } from '../../providers/incidencias';

@IonicPage()
@Component({
  selector: 'page-incidencias',
  templateUrl: 'incidencias.html',
})
export class IncidenciasPage {

  incidenciaDetalle = "IncidenciaDetallePage";
  contrarefencia = "IncidenciaContrareferenciaModalPage";
  referencia = "IncidenciaFotoModalPage";

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private modalCtrl: ModalController,
              private _is: IncidenciasService) {
  }

  presentModal(page: any) {
    let modal = this.modalCtrl.create(page);
    modal.present();
  }

}
